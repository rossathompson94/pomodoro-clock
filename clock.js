$(document).ready(function(){
  $('#reset').hide();
  var count = parseInt($('#currentTime').html());
  var breakCount = parseInt($('#currentBreak').html());


  $('#subtract1').click(function(){
    if(count>1) {
      count -= 1;
      $('#currentTime').html(count);
    }

  });

  $('#add1').click(function(){
    count += 1;
    $('#currentTime').html(count);
  });

  $('#subtract2').click(function(){
    if(breakCount>1) {
      breakCount -= 1;
      $('#currentBreak').html(breakCount);
    }
  });

  $('#add2').click(function(){
    breakCount += 1;
    $('#currentBreak').html(breakCount);
  });
});
